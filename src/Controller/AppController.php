<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Routing\Router;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
    public $codeSuccess200 = 200;
    public $codeSuccessCreate201 = 201;
    public $codeInvalid400 = 400;
    public $codeUnauthorized401 = 401;
    public $codeNotFound404 = 404;
    public $codeInvalidMethod405 = 405;
    public $codeConflictRecord409 = 409;

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('Flash', ['clear' => true]);

        /*
         * Enable the following component for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        $this->loadComponent('Security');

        $this->loadComponent('Auth', [
            'authorize'=> 'Controller',
            'authenticate' => [
                'Form' => [
                    'finder' => 'auth'
                ]
            ],
            'loginAction' => [
                'controller' => 'Users',
                'action' => 'login'
            ],
            'loginRedirect' => [
                'controller' => 'Posts',
                'action' => 'index'
            ],
            'unauthorizedRedirect' => $this->referer()
        ]);

        // Allow the display action so our pages controller
        // continues to work. Also enable the read only actions.
        // $this->Auth->allow(['display', 'view', 'index']);
    }

    public function isAuthorized($user)
    {
        return true;
    }

    public function beforeFilter(Event $event) 
    {
        $this->set('authUser', $this->Auth->user());
        $this->Security->blackHoleCallback = 'blackhole';
    }

    public function blackhole($type) 
    {
        $this->redirect($this->referer());
    }

    public function renderErrors(array $errors = []) 
    {
        $errMsg = '';
        foreach ($errors as $key => $error) {
            foreach ($error as $key => $msg) {
                $errMsg .= $msg."<br>";
            }
        }
        
        return $errMsg;
    }

    protected function getCleanData() : array
    {
        $data = $this->request->getData();

        array_walk_recursive($data, function (&$item, $key) {
            if (is_string($item)) {
                $item = trim($item);    
            }
        });

        return $data;
    }

    protected function baseUrl() : string
    {
        $baseUrl = Router::url('/', true);

        return $baseUrl;
    }
}
