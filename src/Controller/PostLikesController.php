<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Http\Client;

/**
 * Post Likes Controller
 *
 * @property \App\Model\Table\PostLikesTable $PostLikes
 */
class PostLikesController extends AppController
{

    public function initialize()
    {
        parent::initialize();
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Security->setConfig('unlockedActions', ['like']);
    }
    
    /**
     * Like method
     *
     * This method tags certain posts to liked when triggered by the user.
     *
     * @return json
     */
    public function like() 
    {
        $status = 'error';
        $message = '';
        $data = $this->getCleanData();

        $data['userId'] = $this->Auth->User('id');
        $accessToken = $this->Auth->User('token');

        $http = new Client(
            [
            'headers' => ['Authorization' => 'Bearer ' . $accessToken]
            ]
        );
        
        $response = $http->put(
            $this->baseUrl().'/api/PostLikes/like', $data,
            ['type' => 'json']
        );
        $result = $response->getJson();

        if ($result['code'] == $this->codeSuccess200) {
            $status = 'success';
        } else {
            $message = $result['message'];
        }

        return $this->response
            ->withType('application/json')
            ->withStringBody(
                json_encode(
                    [
                    'status' => $status,
                    'message' => $message
                    ]
                )
            );
    }
}
