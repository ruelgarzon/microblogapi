<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Mailer\Email;
use Cake\Utility\Security;
use Cake\Http\Client;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{

    public $cryptKey = 'wt1U5MACWJFTXGenFoZoiLwQGrLgdbHA';

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['add', 'activate']);
    }

    /**
     * Add method
     *
     * @return Redirects to Login on successful add, renders view otherwise.
     */
    public function add()
    {
        if ($this->Auth->user()) {

            return $this->redirect(['controller' => 'posts','action' => 'index']);
        }
        if ($this->request->is('post')) {
            $data = $this->getCleanData();

            $http = new Client();

            $response = $http->post(
                $this->baseUrl().'/api/users/add', $data,
                ['type' => 'json']
            );
            $result = $response->getJson();

            if ($result['code'] == $this->codeSuccessCreate201) {
                $newUser = $result['data']['newUser'];
                extract($newUser);
                $this->sendAccountActivation($email, $id, $first_name);
                $this->Flash->success(__('Your account has been successfully submitted. Please check your email to activate.'));

                return $this->redirect(['action' => 'login']);
            }
            
            $errorMsgs = $result['message'];
            $this->Flash->error($errorMsgs);
        }
        $this->set('user', 'Users');
    }

    /**
     * Edit method
     *
     * @param  int $data['id'] User id.
     * @return Redirects on successful edit, renders error view otherwise.
     * @throws NotFoundException When record not found.
     */
    public function edit()
    {
        $data = $this->getCleanData();

        if ($this->request->is(['post'])) {

            if ($data['profile_picture']['name'] != '' ) {

                $extn = substr(
                    $data['profile_picture']['name'], 
                    strrpos($data['profile_picture']['name'], '.')+1
                );
                $image = "user_" . $this->Auth->user('id'). "_" . date('His') .  "." . $extn;
                $img = WWW_ROOT . 'img/profiles/' . $image;
                
                move_uploaded_file($data['profile_picture']['tmp_name'], $img);

                $data['profile_picture'] = $image;
            } else {
                unset($data['profile_picture']);
            }

            $accessToken = $this->Auth->User('token');

            $http = new Client(
                [
                'headers' => ['Authorization' => 'Bearer ' . $accessToken]
                ]
            );

            $response = $http->patch(
                $this->baseUrl().'/api/users/edit', $data,
                ['type' => 'json']
            );
            $result = $response->getJson();

            if ($result['code'] == $this->codeSuccessCreate201) {
                $newUser = $result['data']['newUser'];
                $newUser['token'] = $this->Auth->User('token');
                $this->Auth->setUser($newUser);
                $this->Flash->success(__('Profile changes have been saved'));

            } else {
                
                $errorMsgs = $result['message'];
                $this->Flash->error($errorMsgs);
            }

            return $this->redirect(['controller' => 'posts','action' => 'index']);
        }
        $user = 'Users';
        $this->set(compact('user'));
    }

    /**
     * Change Password method
     *
     * @param  int $data['id'] User id.
     * @return Redirects on successful update, renders error view otherwise.
     * @throws NotFoundException When record not found.
     */
    public function changePassword()
    {
        $data = $this->getCleanData();

        if ($this->request->is(['post'])) {

            $accessToken = $this->Auth->User('token');

            $http = new Client(
                [
                'headers' => ['Authorization' => 'Bearer ' . $accessToken]
                ]
            );

            $response = $http->patch(
                $this->baseUrl().'/api/users/changePassword', $data,
                ['type' => 'json']
            );
            $result = $response->getJson();
            if ($result['code'] == $this->codeSuccessCreate201) {
                $this->Flash->success(__('New password has been saved'));
            } else {
                
                $errorMsgs = $result['message'];
                $this->Flash->error($errorMsgs);
            }

            return $this->redirect(['controller' => 'posts','action' => 'index']);
        }
    }

    /**
     * Activate method
     *
     * This method activates the newly signed up user.
     *
     * @param  string $encryptedId Encrypted User id.
     * @return Redirects on successful transaction, renders error view otherwise.
     * @throws NotFoundException When record not found.
     */
    public function activate(string $encryptedId = null) 
    {
        $encryptedId = base64_decode(strtr($encryptedId, '._-', '+/='));
        $id = Security::decrypt($encryptedId, $this->cryptKey);
        
        $http = new Client();

        $response = $http->patch(
            $this->baseUrl().'/api/users/activate/'.$id, [],
            ['type' => 'json']
        );
        $result = $response->getJson();

        if ($result['code'] == $this->codeSuccessCreate201) {
            $this->Flash->success(__('Congratulations! Your account has been successfully activated.'));
        } else {
            
            $errorMsgs = $result['message'];
            $this->Flash->error($errorMsgs);
        }
            

        return $this->redirect(['action' => 'login']);
    }

    /**
     * Login method
     *
     * This method authenticates user credentials.
     *
     * @return Redirects on successful transaction, renders error view otherwise.
     */
    public function login()
    {
        if ($this->Auth->user()) {

            return $this->redirect(['controller' => 'posts','action' => 'index']);
        }
        if ($this->request->is('post')) {
            $http = new Client();

            $data = $this->getCleanData();

            $response = $http->post(
                $this->baseUrl().'/api/users/login', $data,
                ['type' => 'json']
            );
            $result = $response->getJson();

            if ($result['code'] == $this->codeSuccess200) {
                $user = $result['data']['user'];
                $user['token'] = $result['data']['token'];
                $this->Auth->setUser($user);

                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error('Your username or password is incorrect.');
        }
    }

    /**
     * Logout method
     *
     * This method destroys or end current user session and requires re-authentication for
     *      another transaction.
     *
     * @return Redirects to login page.
     */
    public function logout()
    {
        $this->Flash->success('You are now logged out.');

        return $this->redirect($this->Auth->logout());
    }

    /**
     * Send Account Activation method
     *
     * This method will send the an email of the activation link to the
     *      newly registered user.
     *
     * @param  string $email     User email.
     * @param  int    $id        User id.
     * @param  string $firstName User first name.
     * @return true|false
     */
    private function sendAccountActivation(string $email, int $id, string $firstName) : bool
    {
        try {

            $encryptedId = strtr(base64_encode(Security::encrypt($id, $this->cryptKey)), '+/=', '._-');;

            $Email = new Email('default');
            $Email->setViewVars(['id' => $encryptedId, 'firstName' => $firstName]);
            $Email->viewBuilder()->setTemplate('welcome');
            $Email->viewBuilder()->setHelpers(['Url']);
            $Email->setEmailFormat('html')
                ->setTo($email)
                ->setFrom(['no-reply@cake-microblog.com' => 'Account Verification'])
                ->setSubject('MicroBlog account verification.')
                ->send();

            return true;

        } catch (Exception $e) {
            throw $e;
            
        }
    }

}
