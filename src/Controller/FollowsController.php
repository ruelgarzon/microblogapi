<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Http\Client;

/**
 * Follows Controller
 *
 * @property \App\Model\Table\FollowsTable $Follows
 */
class FollowsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
    }
    
    /**
     * Follow method
     *
     * This method saves a record for follow action and deletes on unfollow
     *
     * @param int $id User id
     *
     * @return Redirects to referer
     */
    public function follow(int $id) 
    {

        $data = [
            'userId' => $this->Auth->User('id'),
            'id' => $id
        ];

        $accessToken = $this->Auth->User('token');

        $http = new Client(
            [
            'headers' => ['Authorization' => 'Bearer ' . $accessToken]
            ]
        );
        
        $response = $http->put(
            $this->baseUrl().'/api/follows/follow', $data,
            ['type' => 'json']
        );
        $result = $response->getJson();

        if ($result['code'] == $this->codeSuccess200) {
            if ($result['data']['followedFlag']) {
                $msg = 'You have followed ';
            } else {
                $msg = 'You have unfollowed ';
            }
            $this->Flash->success(__($msg . h($result['data']['followedName'])));
        } else {
            
            $errorMsgs = $result['message'];
            $this->Flash->error($errorMsgs);
        }

        return $this->redirect($this->referer());
    }

    /**
     * Followers method
     *
     * This method retrieves the list of followers.
     *
     * @param int|null $userId User  id
     *
     * @return \Cake\Http\Response|null
     */
    public function followers(int $userId = null)
    {
        $userId = ($userId) ? $userId : $this->Auth->User('id');
        $sessUid = $this->Auth->User('id');
        $data = [
            'userId' => $userId,
            'sessUid' => $sessUid
        ];

        $accessToken = $this->Auth->User('token');
        $http = new Client(
            [
            'headers' => ['Authorization' => 'Bearer ' . $accessToken]
            ]
        );

        $page = $this->request->getQuery('page');
        $paginate = ($page) ? "?page=$page" : '';
        $response = $http->post(
            $this->baseUrl().'/api/follows/followers'.$paginate, 
            $data,
            ['type' => 'json']
        );
        $result = $response->getJson();

        if ($result['code'] != $this->codeSuccess200) {
            $this->Flash->error(__($result['message']));

            return $this->redirect($this->referer());
        }

        extract($result['data']);
        $pagination['paging'] = $paging;
        
        $this->set(compact('userDetail', 'followers', 'pagination'));
    }

    /**
     * Following method
     *
     * This method retrieves the list of followed users.
     *
     * @param int|null $userId User  id
     *
     * @return \Cake\Http\Response|null
     */
    public function following(int $userId = null)
    {
        $userId = ($userId) ? $userId : $this->Auth->User('id');
        $sessUid = $this->Auth->User('id');
        $data = [
            'userId' => $userId,
            'sessUid' => $sessUid
        ];

        $accessToken = $this->Auth->User('token');
        $http = new Client(
            [
            'headers' => ['Authorization' => 'Bearer ' . $accessToken]
            ]
        );

        $page = $this->request->getQuery('page');
        $paginate = ($page) ? "?page=$page" : '';
        $response = $http->post(
            $this->baseUrl().'/api/follows/following'.$paginate, 
            $data,
            ['type' => 'json']
        );
        $result = $response->getJson();

        if ($result['code'] != $this->codeSuccess200) {
            $this->Flash->error(__($result['message']));

            return $this->redirect($this->referer());
        }

        extract($result['data']);
        $pagination['paging'] = $paging;
        
        $this->set(compact('userDetail', 'followings', 'pagination'));
    }
}
