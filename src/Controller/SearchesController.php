<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Http\Client;

/**
 * Searches Controller
 */
class SearchesController extends AppController
{

    public function initialize()
    {
        parent::initialize();
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }
    
    /**
     * Index method
     *
     * This method display the search results based on selected type and keyword.
     * This can either displays users or posts.
     *
     * @return \Cake\Http\Response|null
     */
    public function index() 
    {
        $type = '';
        $keyword = '';
        $data = $this->getCleanData();

        if (isset($data['type'])) {
            $type = $data['type'];
            $this->getRequest()->getSession()->delete('SearchKeyword');
        } else {
            if($this->getRequest()->getSession()->check('SearchType')) {
                $type = $this->getRequest()->getSession()->read('SearchType');
            } else {
                $type = 'user';
            }
        }
        if (isset($data['keyword'])) {
            $keyword = $data['keyword'];
        } else {
            if($this->getRequest()->getSession()->check('SearchKeyword')) {
                $keyword = $this->getRequest()->getSession()->read('SearchKeyword');
            }
        }

        $this->getRequest()->getSession()->write('SearchType', $type);
        $this->getRequest()->getSession()->write('SearchKeyword', $keyword);

        $this->set(compact('keyword'));

        $sessUid = $this->Auth->User('id');
        $data = [
            'keyword' => $keyword,
            'sessUid' => $sessUid
        ];

        if ($type == 'post') {
            $renderForm = 'posts';

            $accessToken = $this->Auth->User('token');
            $http = new Client(
                [
                'headers' => ['Authorization' => 'Bearer ' . $accessToken]
                ]
            );

            $page = $this->request->getQuery('page');
            $paginate = ($page) ? "?page=$page" : '';
            $response = $http->post(
                $this->baseUrl().'/api/searches/filterPosts'.$paginate, 
                $data,
                ['type' => 'json']
            );
            $result = $response->getJson();

            if ($result['code'] != $this->codeSuccess200) {
                $this->Flash->error(__($result['message']));

                return $this->redirect($this->referer());
            }

            extract($result['data']);
            $pagination['paging'] = $paging;
            
            $this->set(compact('posts', 'comments', 'pagination'));
        } else {
            $renderForm = 'users';

            $accessToken = $this->Auth->User('token');
            $http = new Client(
                [
                'headers' => ['Authorization' => 'Bearer ' . $accessToken]
                ]
            );

            $page = $this->request->getQuery('page');
            $paginate = ($page) ? "?page=$page" : '';
            $response = $http->post(
                $this->baseUrl().'/api/searches/filterUsers'.$paginate, 
                $data,
                ['type' => 'json']
            );
            $result = $response->getJson();

            if ($result['code'] != $this->codeSuccess200) {
                $this->Flash->error(__($result['message']));

                return $this->redirect($this->referer());
            }

            extract($result['data']);
            $pagination['paging'] = $paging;
            
            $this->set(compact('users', 'followed', 'pagination'));
        }
        
        $this->render($renderForm);
    }
}
