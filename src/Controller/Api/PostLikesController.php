<?php
namespace App\Controller\Api;

use Cake\Http\Exception\UnauthorizedException;
use Cake\Http\Exception\NotFoundException;

/**
 * Post Likes Controller
 *
 * @property \App\Model\Table\PostLikesTable $PostLikes
 */
class PostLikesController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('PostLikes');
    }
    
    /**
     * Like method
     *
     * This method tags certain posts to liked when triggered by the user.
     *
     * @return json
     */
    public function like() 
    {
        $this->request->allowMethod(['post', 'put']);

        $code = null;
        $message = null;
        $likedFlag = true;
        $data = $this->request->getData();

        $id = $data['post_id'];
        $userId = $data['userId'];


        $result = $this->PostLikes->findByPost_idAndUser_id($id, $userId)
            ->first();
        
        if($result) {
            if ($this->PostLikes->delete($result)) {
                $likedFlag = false;
                $code = $this->codeSuccess200;
                $message = 'OK';
            } else {
                $code = $this->codeInvalid400;
                $message = $this->renderErrors($result->getErrors());
            }
        } else {
            $like = $this->PostLikes->newEntity();
            $data = [
                'user_id' => $userId,
                'post_id' => $id
            ];
            $like = $this->PostLikes->patchEntity($like, $data);

            if ($this->PostLikes->save($like)) {
                $code = $this->codeSuccess200;
                $message = 'OK';
            } else {
                $code = $this->codeInvalid400;
                $message = $this->renderErrors($like->getErrors());
            }
        }

        $data = [
            'likedFlag' => $likedFlag
        ];

        $this->set(compact('code', 'message', 'data'));
        $this->set('_serialize', ['code', 'message', 'data']);
    }
}
