<?php
namespace App\Controller\Api;

use Cake\Event\Event;
use Cake\Http\Exception\UnauthorizedException;
use Cake\Http\Exception\NotFoundException;
use Cake\Utility\Security;
use Firebase\JWT\JWT;
use Cake\Http\ServerRequest;
use Cake\I18n\Time;
use Cake\Mailer\Email;

class UsersController extends AppController
{
    public $cryptKey = 'wt1U5MACWJFTXGenFoZoiLwQGrLgdbHA';
    
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['login', 'add', 'activate', 'sendEmail']);
    }

    /**
     * Login method
     *
     * This method authenticates user credentials.
     *
     * @return json.
     */
    public function login()
    {
        $this->request->allowMethod(['post']);
        $user = $this->Auth->identify();
        if (!$user) {
            throw new UnauthorizedException("Your username or password is incorrect");
        }else{
            $tokenId  = base64_encode(32);
            $issuedAt = time();
            $key = Security::getSalt();
            $this->set(
                [
                'code' => $this->codeSuccess200,
                'message' => 'Login successful',
                'data' => [
                    'token' => JWT::encode(
                        [
                        'alg' => 'HS256',
                        'id' => $user['id'],
                        'sub' => $user['id'],
                        'iat' => time(),
                        'exp' =>  time() + 86400,
                        ],
                        $key
                    ),
                    'user' => $user
                ],
                '_serialize' => ['code', 'messages', 'data']
                ]
            );
        }
    }

    /**
     * Add method
     *
     * @return json.
     */
    public function add()
    {
        $this->request->allowMethod(['post', 'put']);
        $code = null;
        $message = null;
        $user = $this->Users->newEntity($this->request->getData());
        $newUser = $this->Users->save($user);

        if ($newUser) {
            $code = $this->codeSuccessCreate201;
            $message = 'The user has been saved.';
        } else {
            $code = $this->codeInvalid400;
            $message = $this->renderErrors($user->getErrors());
        }
        $data = [];
        $data['newUser'] = $newUser;

        $this->set(compact('code', 'message', 'data'));
        $this->set('_serialize', ['code', 'message', 'data']);
    }

     /**
      * Activate method
      *
      * @param  string|null $id User id.
      * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
      * @throws \Cake\Network\Exception\NotFoundException When record not found.
      */
    public function activate($id = null)
    {
        $this->request->allowMethod(['patch', 'post', 'put']);
        $user = $this->Users->get($id);

        $code = null;
        $message = null;
        $data = [];

        if (! $user) {
            throw new NotFoundException(__('Invalid user'));
        } 
        if($user->activated) {
            $code = $this->codeConflictRecord409;
            $message = 'Invalid action! Your account is already active.';
        } else {
            $user->activated = 1;
            if ($this->Users->save($user)) {
                $message = 'The user has been saved.';
                $code = $this->codeSuccessCreate201;
            } else {
                $code = $this->codeInvalid400;
                $message = $this->renderErrors($user->getErrors());
            }
        }
        $this->set(compact('code', 'message', 'data'));
        $this->set('_serialize', ['code', 'message', 'data']);

    }

    /**
     * Edit method
     *
     * @param  string|null $id User id.
     * @return json.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit()
    {
        $this->request->allowMethod(['patch', 'post', 'put']);
        $data = $this->request->getData();
        $user = $this->Users->get($data['id']);

        $code = null;
        $message = null;

        if (!$user) {
            throw new NotFoundException(__('Invalid action.'));
        }

        if (isset($data['postAttachment'])) {
            $img = $data['postAttachment']['value'];
            $img = explode(',', $img);
            $imageFile = base64_decode($img[1]);

            $extn = substr(
                    $data['postAttachment']['filename'], 
                    strrpos($data['postAttachment']['filename'], '.')+1
                );

            $fileName = "user_" . $data['id'] . "_" . date('His') .  "." . $extn;

            $file_dir = WWW_ROOT . 'img/profiles/' . $fileName;
            file_put_contents($file_dir, $imageFile);

            $data['profile_picture'] = $fileName;
        }

        $user = $this->Users->patchEntity($user, $data);
        $newUser = $this->Users->save($user);

        if ($newUser) {
            $code = $this->codeSuccessCreate201;
            $message = 'The user has been updated.';
        } else {
            $code = $this->codeInvalid400;
            $message = $this->renderErrors($user->getErrors());
        }
        
        $data = [];
        $data['newUser'] = $newUser;

        $this->set(compact('code', 'message', 'data'));
        $this->set('_serialize', ['code', 'message', 'data']);
    }

    /**
     * Change Password method
     *
     * @param  string|null $id User id.
     * @return json.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function changePassword()
    {
        $this->request->allowMethod(['patch', 'post', 'put']);

        $code = null;
        $message = null;

        $data = $this->request->getData();
        $user = $this->Users->get($data['id']);
        if (!$user) {
            throw new NotFoundException(__('Invalid action.'));
        }
        $user = $this->Users->patchEntity($user, $data, ['validate' => 'changePass']);

        if ($this->Users->save($user)) {
            $message = 'The user changes has been saved.';
            $code = $this->codeSuccessCreate201;
        } else {
            $code = $this->codeInvalid400;
            $message = $this->renderErrors($user->getErrors());
        }

        $data = [];

        $this->set(compact('code', 'message', 'data'));
        $this->set('_serialize', ['code', 'message', 'data']);
    }

    /**
     * Send Account Activation method
     *
     * This method will send the an email of the activation link to the
     *      newly registered user.
     *
     * @param  string $email     User email.
     * @param  int    $id        User id.
     * @param  string $firstName User first name.
     * @return true|false
     */
    public function sendEmail()
    {
        try {

            $this->request->allowMethod(['patch', 'post', 'put']);
            $data = $this->request->getData();

            $email = $data['email'];
            $id = $data['id'];
            $firstName = $data['firstName'];

            $code = null;
            $message = null;

            $encryptedId = strtr(base64_encode(Security::encrypt($id, $this->cryptKey)), '+/=', '._-');;

            $Email = new Email('default');
            $Email->setViewVars(['id' => $encryptedId, 'firstName' => $firstName]);
            $Email->viewBuilder()->setTemplate('welcome');
            $Email->viewBuilder()->setHelpers(['Url']);
            $Email->setEmailFormat('html')
                ->setTo($email)
                ->setFrom(['no-reply@cake-microblog.com' => 'Account Verification'])
                ->setSubject('MicroBlog account verification.')
                ->send();

            $data = [];
            $code = $this->codeSuccessCreate201;
            $message = 'Email activation successfully sent.';

            $this->set(compact('code', 'message', 'data'));
            $this->set('_serialize', ['code', 'message', 'data']);

        } catch (Exception $e) {
            throw $e;
            
        }
    }

}
