<?php
namespace App\Controller\Api;

use Cake\Http\Exception\UnauthorizedException;
use Cake\Http\Exception\NotFoundException;
use Cake\ORM\TableRegistry;

/**
 * Follows Controller
 *
 * @property \App\Model\Table\PostCommentsTable $Follows
 */
class FollowsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Users');
        $this->loadModel('Followers');
        $this->loadModel('Followings');
    }
    
    /**
     * Follow method
     *
     * This method saves a record for follow action and deletes on unfollow
     *
     * @return json
     */
    public function follow() 
    {
        $this->request->allowMethod(['post', 'put']);

        $code = null;
        $message = null;
        $data = $this->request->getData();

        $id = $data['id'];
        $userId = $data['userId'];
        $followedUser = $this->Users->get($id);

        $result = $this->Follows->findByFollowed_idAndFollower_id($id, $userId)
            ->first();

        $followedUser = $this->Users->get($id);
        $followedName = $followedUser->first_name.' '.$followedUser->last_name;

        if ($result) {
            if ($this->Follows->delete($result)) {
                $code = $this->codeSuccess200;
                $message = 'OK';
            } else {
                $code = $this->codeInvalid400;
                $message = $this->renderErrors($result->getErrors());
            }
            $followedFlag = false;
        } else {
            $follow = $this->Follows->newEntity();
            $data = [
                'follower_id' => $userId,
                'followed_id' => $id
            ];
            $follow = $this->Follows->patchEntity($follow, $data);

            if ($this->Follows->save($follow)) {
                $code = $this->codeSuccess200;
                $message = 'OK';
            } else {
                $code = $this->codeInvalid400;
                $message = $this->renderErrors($follow->getErrors());
            }
            $followedFlag = true;
        }

        $data = [];
        $data['followedName'] = $followedName;
        $data['followedFlag'] = $followedFlag;

        $this->set(compact('code', 'message', 'data'));
        $this->set('_serialize', ['code', 'message', 'data']);
    }

    /**
     * Followers method
     *
     * This method retrieves the list of followers
     *
     * @return json
     */
    public function followers()
    {
        $this->request->allowMethod(['post']);

        $code = null;
        $message = null;
        $data = $this->request->getData();

        $userId = $data['userId'];
        $sessUid = $data['sessUid'];

        $userDetail = $this->Users->get($userId);

        if (!$userDetail) {
            throw new NotFoundException(__('The requested user account is not available.'));
        }

        $userIdArray[] = $userId;

        $this->paginate = [
            'contain' => ['Users.UserFollowing'],
            'conditions' => [
                'Followers.followed_id' => $userId
            ],
            'limit' => 10 
        ];
        $followers = $this->paginate($this->Followers);

        $paging = $this->request->getParam('paging');

        $data = [
            'followers' => $followers,
            'userDetail' => $userDetail,
            'paging' => $paging
        ];

        $code = $this->codeSuccess200;
        $message = 'OK';

        $this->set(compact('code', 'message', 'data'));
        $this->set('_serialize', ['code', 'message', 'data']);
    }

    /**
     * Following method
     *
     * This method retrieves the list of followed users
     *
     * @return json
     */
    public function following()
    {
        $this->request->allowMethod(['post']);

        $code = null;
        $message = null;
        $data = $this->request->getData();

        $userId = $data['userId'];
        $sessUid = $data['sessUid'];

        $userDetail = $this->Users->get($userId);

        if (!$userDetail) {
            throw new NotFoundException(__('The requested user account is not available.'));
        }

        $userIdArray[] = $userId;

        $this->paginate = [
            'contain' => ['Users.UserFollowing'],
            'conditions' => [
                'Followings.follower_id' => $userId
            ],
            'limit' => 10 
        ];
        $followings = $this->paginate($this->Followings);
        
        $paging = $this->request->getParam('paging');

        $data = [
            'followings' => $followings,
            'userDetail' => $userDetail,
            'paging' => $paging
        ];

        $code = $this->codeSuccess200;
        $message = 'OK';

        $this->set(compact('code', 'message', 'data'));
        $this->set('_serialize', ['code', 'message', 'data']);
    }
}
