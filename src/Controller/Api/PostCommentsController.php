<?php
namespace App\Controller\Api;

use Cake\Http\Exception\UnauthorizedException;
use Cake\Http\Exception\NotFoundException;

/**
 * Post Comments Controller
 *
 * @property \App\Model\Table\PostCommentsTable $PostComments
 */
class PostCommentsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
    }
    
    /**
     * Write Comment method
     *
     * This method saves post's comments.
     *
     * @return json
     */
    public function writeComment() 
    {
        $this->request->allowMethod(['post', 'put']);

        $code = null;
        $message = null;
        $data = $this->request->getData();

        $comment = $this->PostComments->newEntity();
        $comment = $this->PostComments->patchEntity($comment, $data);
        $saveComment = $this->PostComments->save($comment);

        if ($saveComment) {
            $code = $this->codeSuccessCreate201;
            $message = 'Comment saved successfully';
        } else {
            $code = $this->codeInvalid400;
            $message = $this->renderErrors($comment->getErrors());
        }

        $data = [];
        $data['comment'] = $saveComment;

        $this->set(compact('code', 'message', 'data'));
        $this->set('_serialize', ['code', 'message', 'data']);
    }

    /**
     * Edit Comment method
     *
     * This method saves user inputed revision on comments.
     *
     * @return json
     */
    public function editComment() 
    {
        $this->request->allowMethod(['post', 'put']);

        $code = null;
        $message = null;
        $data = $this->request->getData();

        $comment = $this->PostComments->get($data['id']);
        if (!$comment) {
            throw new NotFoundException(__('Invalid action.'));
        }

        $comment = $this->PostComments->patchEntity($comment, $data);

        if ($this->PostComments->save($comment)) {
            $code = $this->codeSuccess200;
            $message = 'Comment updated successfully';
        } else {
            $code = $this->codeInvalid400;
            $message = $this->renderErrors($comment->getErrors());
        }

        $data = [];

        $this->set(compact('code', 'message', 'data'));
        $this->set('_serialize', ['code', 'message', 'data']);
    }


    /**
     * Edit Comment method
     *
     * This method saves user inputed revision on comments.
     *
     * @return json
     */
    public function deleteComment() 
    {
        $this->request->allowMethod(['post', 'put']);

        $code = null;
        $message = null;
        $data = $this->request->getData();

        $comment = $this->PostComments->get($data['id']);
        if (!$comment) {
            throw new NotFoundException(__('Invalid action.'));
        }

        $comment->deleted = 1;

        if ($this->PostComments->save($comment)) {
            $code = $this->codeSuccess200;
            $message = 'Comment deleted successfully';
        } else {
            $code = $this->codeInvalid400;
            $message = $this->renderErrors($comment->getErrors());
        }

        $data = [];

        $this->set(compact('code', 'message', 'data'));
        $this->set('_serialize', ['code', 'message', 'data']);
    }
}
