<?php
namespace App\Controller\Api;

use Cake\Http\Exception\UnauthorizedException;
use Cake\Http\Exception\NotFoundException;
use Cake\Log\Log;
/**
 * Posts Controller
 *
 * @property \App\Model\Table\PostsTable $Posts
 */
class PostsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
        $this->loadModel('Users');
        $this->loadModel('PostComments');
        $this->loadModel('PostLikes');
        $this->loadModel('Follows');
        $this->loadModel('Followers');
        $this->loadModel('Followings');
    }

    /**
     * Index method
     *
     * This method display the home page of the user after login. This also displays
     *  posts of the logged in user and the posts of the users he/she follows.
     *
     * @param int|null $userId User  id
     *
     * @return \Cake\Http\Response|null
     */
    public function getIndexContent(int $userId = null, int $sessUid = null)
    {
        $code = null;
        $message = null;

        $userDetail = $this->Users->find('all')
            ->where(
                [
                'activated' => 1,
                'id' => $userId
                ]
            )
            ->first();

        if (!$userDetail) {
            throw new NotFoundException(__('The requested user account is not available.'));
        }

        $followedThisUser = $this->checkFollowing($userId, $sessUid);
        $followers = $this->getFollower($userId, $sessUid);
        $followings = $this->getFollowing($userId, $sessUid);
        $followerCount = $this->getFollowerCount($userId);
        $followingCount = $this->getFollowingCount($userId);

        if ($userId == $sessUid) {
            $userIdArray = $this->getFollowedIds($userId);
        }

        $userIdArray[] = $userId;
        
        $this->paginate = [
            'contain' => ['Users', 'Reposts.Users'],
            'conditions' => [
                'Posts.user_id IN' => $userIdArray,
                'Posts.deleted' => 0,
            ],
            'limit' => 10,
            'order' => ['Posts.created' => 'desc']  
        ];
        $posts = $this->paginate();
        $comments = $this->getPostComments($posts, $sessUid);
        $paging = $this->request->getParam('paging');

        $data = [
            'followers' => $followers,
            'followings' => $followings,
            'followerCount' => $followerCount,
            'followingCount' => $followingCount,
            'followedThisUser' => $followedThisUser,
            'userDetail' => $userDetail,
            'comments' => $comments,
            'posts' => $posts,
            'paging' => $paging
        ];

        $code = $this->codeSuccessCreate201;
        $message = 'OK';

        $this->set(compact('code', 'message', 'data'));
        $this->set('_serialize', ['code', 'message', 'data']);

    }

    /**
     * Get PostComments method
     *
     * This method fetches and builds each post's comments.
     *
     * @param object $posts User's posts dataset
     *
     * @return array
     */
    private function getPostComments(object $posts, int $sessUid) : array
    {
        $comments = [];
        foreach ($posts as $key => $post) {
            $postId = $post['id'];

            $comments[$key]['comments'] = $this->PostComments->find()
                ->where(
                    [
                    'PostComments.post_id' => $postId,
                    'PostComments.deleted' => 0
                    ]
                )
                ->contain('Users')
                ->toArray();
            $comments[$key]['commentCount'] = $this->PostComments->find()
                ->where(
                    [
                    'post_id' => $postId,
                    'deleted' => 0
                    ]
                )
                ->count();

            $comments[$key]['likeCount'] = $this->PostLikes->find()
                ->where(['post_id' => $postId])
                ->count();

            $result = $this->PostLikes->findByPost_idAndUser_id($postId, $sessUid)
                ->first();
            $comments[$key]['userLiked'] = ($result) ? true : false;

        }

        return $comments;
    }

    /**
     * Get Followed Id's method
     *
     * This method fetches all id's of the user followed accounts.
     *
     * @param in $userId User id
     *
     * @return array
     */
    private function getFollowedIds(int $userId) : array
    {
        $query = $this->Follows->find(
            'list', [
                'keyField' => 'id',
                'valueField' => 'followed_id'
            ]
        )
            ->where(['follower_id' => $userId])
            ->toArray();

        return $query;
    }

    /**
     * Check Following method
     *
     * This method checks if the viewed user has been followed by the currently logged in user.
     *
     * @param int $userId  User id
     * @param int $sessUid User id of the currently logged in account
     *
     * @return bool
     */
    private function checkFollowing(int $userId, int $sessUid) : bool
    {

        $query = $this->Follows->findAllByFollowed_idAndFollower_id($userId, $sessUid);
        $result = $query->toArray();

        $return = ($result) ? true : false;

        return $return;
    }

    /**
     * Get Follower method
     *
     * This method returns the user's list of followers.
     *
     * @param int $userId  User id
     * @param int $sessUid User id of the currently logged in account
     *
     * @return array
     */
    private function getFollower(int $userId, int $sessUid) : array
    {
        $query = $this->Followers->find()
            ->distinct(['Followers.id','UserFollowing.id'])
            ->where(['Followers.followed_id' => $userId])
            ->contain('Users.UserFollowing')
            ->limit(10)
            ->toArray();

        return $query;
    }

    /**
     * Get Following method
     *
     * This method returns the user's list of followed user accounts.
     *
     * @param int $userId  User id
     * @param int $sessUid User id of the currently logged in account
     *
     * @return array
     */
    private function getFollowing(int $userId, int $sessUid) : array
    {
        $query = $this->Followings->find()
            ->distinct(['Followings.id', 'UserFollowing.id'])
            ->where(['Followings.follower_id' => $userId])
            ->contain('Users.UserFollowing')
            ->limit(10)
            ->toArray();

        return $query;
    }

    /**
     * Get Follower Count method
     *
     * This method returns the user's total number of followers.
     *
     * @param int $userId User id
     *
     * @return int
     */
    private function getFollowerCount(int $userId) : int
    {
        $query = $this->Followers->find()
            ->where(['followed_id' => $userId])
            ->count();

        return $query;
    }

    /**
     * Get Following Count method
     *
     * This method returns the user's total number of followed user accounts.
     *
     * @param int $userId User id
     *
     * @return int
     */
    private function getFollowingCount(int $userId) : int
    {
        $query = $this->Followings->find()
            ->where(['follower_id' => $userId])
            ->count();

        return $query;
    }

    /**
     * Add method
     *
     * @return Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->request->allowMethod(['post', 'put']);

        $code = null;
        $message = null;
        $data = $this->request->getData();

        if (isset($data['postAttachment'])) {
            $img = $data['postAttachment']['value'];
            $img = explode(',', $img);
            $imageFile = base64_decode($img[1]);

            $extn = substr(
                    $data['postAttachment']['filename'], 
                    strrpos($data['postAttachment']['filename'], '.')+1
                );

            $fileName = "user_" . $data['user_id'] . "_" . date('His') .  "." . $extn;

            $file_dir = WWW_ROOT . 'img/posts/' . $fileName;
            file_put_contents($file_dir, $imageFile);

            $data['image_name'] = $fileName;
        }

        $post = $this->Posts->newEntity($data);

        if ($this->Posts->save($post)) {
            $code = $this->codeSuccessCreate201;
            $message = 'The post has been saved.';
        } else {
            $code = $this->codeInvalid400;
            $message = $this->renderErrors($post->getErrors());
        }

        $data = [];

        $this->set(compact('code', 'message', 'data'));
        $this->set('_serialize', ['code', 'message', 'data']);
    }


    /**
     * Edit method
     *
     * @param  int $data['id'] Post id.
     * @return Redirects on successful edit, renders error view otherwise.
     * @throws NotFoundException When record not found.
     */
    public function edit()
    {
        $this->request->allowMethod(['post', 'put', 'patch']);

        $code = null;
        $message = null;
        $data = $this->request->getData();
        $post = $this->Posts->get($data['id']);
        
        if (!$post) {
            throw new NotFoundException(__('Invalid action.'));
        }

        $data['image_name'] = isset($data['image_name']) ? $data['image_name'] : null;

        if (isset($data['postAttachment'])) {
            $img = $data['postAttachment']['value'];
            $img = explode(',', $img);
            $imageFile = base64_decode($img[1]);

            $extn = substr(
                    $data['postAttachment']['filename'], 
                    strrpos($data['postAttachment']['filename'], '.')+1
                );

            $fileName = "user_" . $data['user_id'] . "_" . date('His') .  "." . $extn;

            $file_dir = WWW_ROOT . 'img/posts/' . $fileName;
            file_put_contents($file_dir, $imageFile);

            $data['image_name'] = $fileName;
        }
        
        $post = $this->Posts->patchEntity($post, $data);

        if ($this->Posts->save($post)) {
            $code = $this->codeSuccessCreate201;
            $message = 'The post has been updated.';
        } else {
            $code = $this->codeInvalid400;
            $message = $this->renderErrors($post->getErrors());
        }

        $data = [];

        $this->set(compact('code', 'message', 'data'));
        $this->set('_serialize', ['code', 'message', 'data']);
    }

    /**
     * Delete method
     *
     * @param  int $id Post id.
     * @return Redirects on successful update, renders error view otherwise.
     * @throws NotFoundException When record not found.
     */
    public function delete() 
    {
        $this->request->allowMethod(['post', 'put', 'patch']);

        $code = null;
        $message = null;
        $data = $this->request->getData();
        $post = $this->Posts->get($data['id']);

        if ($post->user_id != $data['userId']) {
            throw new UnauthorizedException(__('Invalid action.'));
        }

        $post->deleted = 1;

        if ($this->Posts->save($post)) {
            $code = $this->codeSuccess200;
            $message = 'The post has been deleted.';
        } else {
            $code = $this->codeInvalid400;
            $message = $this->renderErrors($post->getErrors());
        }

        $data = [];

        $this->set(compact('code', 'message', 'data'));
        $this->set('_serialize', ['code', 'message', 'data']);
    }

    /**
     * Repost method
     *
     * @return Redirects on successful save, renders view otherwise.
     */
    public function repost()
    {
        $this->request->allowMethod(['post', 'put']);

        $code = null;
        $message = null;
        $data = $this->request->getData();

        $post = $this->Posts->newEntity();
        $post = $this->Posts->patchEntity($post, $data);

        if ($this->Posts->save($post)) {
            $code = $this->codeSuccessCreate201;
            $message = 'The post has been saved';
        } else {
            $code = $this->codeInvalid400;
            $message = $this->renderErrors($post->getErrors());
        }

        $data = [];

        $this->set(compact('code', 'message', 'data'));
        $this->set('_serialize', ['code', 'message', 'data']);
    }

}
