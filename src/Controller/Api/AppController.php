<?php
namespace App\Controller\Api;

use Cake\Controller\Controller;
use Cake\Event\Event;

class AppController extends Controller
{
    public $codeSuccess200 = 200;
    public $codeSuccessCreate201 = 201;
    public $codeInvalid400 = 400;
    public $codeUnauthorized401 = 401;
    public $codeNotFound404 = 404;
    public $codeInvalidMethod405 = 405;
    public $codeConflictRecord409 = 409;

    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent(
            'Auth', [
            'storage' => 'Memory',
            'authenticate' => [
                'Form' => [
                    'finder' => 'auth'
                ],
                'ADmad/JwtAuth.Jwt' => [
                    'parameter' => 'token',
                    'userModel' => 'Users',
                    'fields' => [
                        'username' => 'id'
                    ],
                    'queryDatasource' => true
                ]
            ],
            'unauthorizedRedirect' => false,
            'checkAuthIn' => 'Controller.initialize'
            ]
        );
    }

    public function beforeFilter(Event $event) {
        if ($this->request->accepts('application/json')) {
            $this->RequestHandler->renderAs($this, 'json');
        }
    }
    
    public function renderErrors(array $errors = []) 
    {
        $errMsg = '';
        foreach ($errors as $key => $error) {
            foreach ($error as $key => $msg) {
                $errMsg .= $msg."<br>";
            }
        }
        
        return $errMsg;
    }
}
