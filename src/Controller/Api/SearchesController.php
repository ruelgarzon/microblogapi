<?php
namespace App\Controller\Api;

use Cake\Http\Exception\UnauthorizedException;
use Cake\Http\Exception\NotFoundException;
use Cake\ORM\TableRegistry;

/**
 * Searches Controller
 *
 * @property \App\Model\Table\SearchesTable $Searches
 */
class SearchesController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Users');
        $this->loadModel('Follows');
        $this->loadModel('Posts');
        $this->loadModel('PostComments');
        $this->loadModel('PostLikes');
    }
    
    /**
     * Get Users List method
     *
     * This method returns the list of active users depending on the provided keyword filter.
     *
     * @param string $keyword Name of the users
     *
     * @return json
     */
    public function filterUsers()
    {

        $this->request->allowMethod(['post']);

        $code = null;
        $message = null;
        $data = $this->request->getData();

        $keyword = isset($data['keyword']) ? $data['keyword'] : '';
        $sessUid = $data['sessUid'];

        $this->paginate = [
            'conditions' => [
                'activated' => 1,
                'OR' => [
                    ["CONCAT(first_name,' ',last_name) LIKE" => "%{$keyword}%"],
                    ['username LIKE' => "%{$keyword}%"]
                ]
            ],
            'limit' => 10,
            'order' => ['created' => 'desc']  
        ];
        $users = $this->paginate($this->Users);
        $followed = $this->checkUsersFollowing($users, $sessUid);

        $paging = $this->request->getParam('paging');

        $data = [
            'users' => $users,
            'followed' => $followed,
            'paging' => $paging
        ];

        $code = $this->codeSuccess200;
        $message = 'OK';

        $this->set(compact('code', 'message', 'data'));
        $this->set('_serialize', ['code', 'message', 'data']);
    }

    /**
     * Check User Following method
     *
     * This method checks if the specifid user has been followed by the currently logged in user.
     *
     * @param object $users
     *
     * @return array
     */
    private function checkUsersFollowing(object $users, int $sessUid) : array
    {
        $followed = [];
        
        foreach ($users as $key => $user) {
            $followed[$key] = $this->checkFollowing($user->id, $sessUid);
        }

        return $followed;
    }

    /**
     * Check Following method
     *
     * This method checks if the viewed user has been followed by the currently logged in user.
     *
     * @param int $userId  User id
     * @param int $sessUid User id of the currently logged in account
     *
     * @return bool
     */
    private function checkFollowing(int $userId, int $sessUid) : bool
    {

        $result = $this->Follows->findAllByFollowed_idAndFollower_id($userId, $sessUid)
            ->toArray();

        $return = ($result) ? true : false;

        return $return;
    }

    /**
     * Get Posts List method
     *
     * This method returns the list of active posts depending on the provided keyword filter.
     *
     * @param string $keyword Contents of the post
     *
     * @return json
     */
    public function filterPosts()
    {
        $this->request->allowMethod(['post']);

        $code = null;
        $message = null;
        $data = $this->request->getData();

        $keyword = ($data['keyword']) ? $data['keyword'] : '';
        $sessUid = $data['sessUid'];

        $this->paginate = [
            'contain' => ['Users', 'Reposts.Users'],
            'conditions' => [
                'Posts.content LIKE' => "%{$keyword}%",
                'Posts.deleted' => 0,
            ],
            'limit' => 10,
            'order' => ['Posts.created' => 'desc']  
        ];
        $posts = $this->paginate($this->Posts);

        $comments = $this->getPostComments($posts, $sessUid);

        $paging = $this->request->getParam('paging');

        $data = [
            'posts' => $posts,
            'comments' => $comments,
            'paging' => $paging
        ];

        $code = $this->codeSuccess200;
        $message = 'OK';

        $this->set(compact('code', 'message', 'data'));
        $this->set('_serialize', ['code', 'message', 'data']);
    }

    /**
     * Get PostComments method
     *
     * This method fetches and builds each post's comments.
     *
     * @param object $posts User's posts dataset
     *
     * @return array
     */
    private function getPostComments(object $posts, int $sessUid) : array
    {
        $comments = [];
        foreach ($posts as $key => $post) {
            $postId = $post['id'];

            $comments[$key]['comments'] = $this->PostComments->find()
                ->where(
                    [
                    'PostComments.post_id' => $postId,
                    'PostComments.deleted' => 0
                    ]
                )
                ->contain('Users')
                ->toArray();
            $comments[$key]['commentCount'] = $this->PostComments->find()
                ->where(
                    [
                    'post_id' => $postId,
                    'deleted' => 0
                    ]
                )
                ->count();

            $comments[$key]['likeCount'] = $this->PostLikes->find()
                ->where(['post_id' => $postId])
                ->count();

            $result = $this->PostLikes->findByPost_idAndUser_id($postId, $sessUid)
                ->first();
            $comments[$key]['userLiked'] = ($result) ? true : false;
        }

        return $comments;
    }
}
