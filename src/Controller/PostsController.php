<?php
namespace App\Controller;

use Cake\Event\Event;
use Cake\Http\Client;

/**
 * Posts Controller
 *
 * @property \App\Model\Table\PostsTable $Posts
 */
class PostsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Security->setConfig('unlockedActions', ['edit','repost']);
    }

    /**
     * Index method
     *
     * This method display the home page of the user after login. This also displays
     *  posts of the logged in user and the posts of the users he/she follows.
     *
     * @param int|null $userId User  id
     *
     * @return \Cake\Http\Response|null
     */
    public function index(int $userId = null)
    {
        $sessUid = $this->Auth->User('id');
        $userId = ($userId) ? $userId : $sessUid;

        $editAccess = ($userId == $sessUid) ? true : false;
        $hideAccess = ($editAccess) ? '' : ' d-none ';

        $accessToken = $this->Auth->User('token');
        $http = new Client(
            [
            'headers' => ['Authorization' => 'Bearer ' . $accessToken]
            ]
        );

        $data = $this->getCleanData();
        $page = $this->request->getQuery('page');
        $paginate = ($page) ? "?page=$page" : '';
        $basePath = $this->baseUrl().'/api/posts/getIndexContent/';
        $response = $http->post(
            $basePath.$userId.'/'.$sessUid.$paginate, 
            $data,
            ['type' => 'json']
        );
        $result = $response->getJson();

        if ($result['code'] != $this->codeSuccessCreate201) {
            $this->Flash->error(__($result['message']));

            return $this->redirect(['controller' => 'posts','action' => 'index']);
        }

        extract($result['data']);
        $pagination['paging'] = $paging;
        $this->set(compact('followers', 'followings', 'followedThisUser'));
        $this->set(compact('followerCount', 'followingCount'));
        $this->set(compact('userDetail', 'hideAccess', 'editAccess'));
        $this->set(compact('comments', 'posts', 'pagination'));
    }

    /**
     * Add method
     *
     * @return Redirects to Login on successful add, renders view otherwise.
     */
    public function add()
    {
        if ($this->request->is('post')) {
            $data = $this->getCleanData();

            if ($data['image_name']['name'] != '' ) {

                $extn = substr(
                    $data['image_name']['name'], 
                    strrpos($data['image_name']['name'], '.')+1
                );
                $sessionId = $this->Auth->user('id');
                $image = "user_" . $sessionId . "_" . date('His') .  "." . $extn;

                $img = WWW_ROOT . 'img/posts/' . $image;
                
                move_uploaded_file($data['image_name']['tmp_name'], $img);

                $data['image_name'] = $image;
            } else {
                unset($data['image_name']);
            }

            $accessToken = $this->Auth->User('token');
            $http = new Client(
                [
                'headers' => ['Authorization' => 'Bearer ' . $accessToken]
                ]
            );
            $data['user_id'] = $this->Auth->user('id');
            $response = $http->post(
                $this->baseUrl().'/api/posts/add', $data,
                ['type' => 'json']
            );
            $result = $response->getJson();

            if ($result['code'] == $this->codeSuccessCreate201) {
                $this->Flash->success(__('Your post has been saved.'));
            } else {
                
                $errorMsgs = $result['message'];
                $this->Flash->error($errorMsgs);
            }
        }

        return $this->redirect($this->referer());
    }

    /**
     * Edit method
     *
     * @return Redirects on successful save, renders view otherwise.
     */
    public function edit()
    {
        $data = $this->getCleanData();
        if ($data['remove_image']) {
            $data['image_name'] = null;
        } else {
            if ($data['image_name']['name'] != '' ) {

                $extn = substr(
                    $data['image_name']['name'], 
                    strrpos($data['image_name']['name'], '.')+1
                );
                $sessionId = $this->Auth->user('id');
                $image = "user_" . $sessionId . "_" . date('His') .  "." . $extn;

                $img = WWW_ROOT . 'img/posts/' . $image;
                
                move_uploaded_file($data['image_name']['tmp_name'], $img);

                $data['image_name'] = $image;
            } else {
                unset($data['image_name']);
            }
        }

        if ($this->request->is(['post'])) {

            $accessToken = $this->Auth->User('token');

            $http = new Client(
                [
                'headers' => ['Authorization' => 'Bearer ' . $accessToken]
                ]
            );

            $response = $http->patch(
                $this->baseUrl().'/api/posts/edit', $data,
                ['type' => 'json']
            );
            $result = $response->getJson();

            if ($result['code'] == $this->codeSuccessCreate201) {
                $this->Flash->success(__('Your post has been updated'));
            } else {
                $errorMsgs = $result['message'];
                $this->Flash->error($errorMsgs);
            }
        }

        return $this->redirect($this->referer());
    }

    /**
     * Delete method
     *
     * @param  int $id Post id.
     * @return Redirects on successful update, renders error view otherwise.
     * @throws NotFoundException When record not found.
     */
    public function delete(int $id) 
    {
        $userId = $this->Auth->User('id');

        $accessToken = $this->Auth->User('token');

        $http = new Client(
            [
            'headers' => ['Authorization' => 'Bearer ' . $accessToken]
            ]
        );

        $data = [
            'id' => $id,
            'userId' => $userId
        ];

        $response = $http->patch(
            $this->baseUrl().'/api/posts/delete', $data,
            ['type' => 'json']
        );
        $result = $response->getJson();
        if ($result['code'] == $this->codeSuccess200) {
            $this->Flash->success(__('Post has been successfully deleted'));
        } else {
            $errorMsgs = $result['message'];
            $this->Flash->error($errorMsgs);
        }

        return $this->redirect($this->referer());
    }

    /**
     * Repost method
     *
     * @return Redirects on successful save, renders view otherwise.
     */
    public function repost()
    {
        $data = $this->getCleanData();
        
        if ($this->request->is('post')) {

            $accessToken = $this->Auth->User('token');
            $http = new Client(
                [
                'headers' => ['Authorization' => 'Bearer ' . $accessToken]
                ]
            );
            $data['user_id'] = $this->Auth->user('id');
            $response = $http->post(
                $this->baseUrl().'/api/posts/add', $data,
                ['type' => 'json']
            );
            $result = $response->getJson();
            if ($result['code'] == $this->codeSuccessCreate201) {
                $this->Flash->success(__('Your post has been saved'));
            } else {
                $errorMsgs = $result['message'];
                $this->Flash->error($errorMsgs);
            }
        }
        
        return $this->redirect($this->referer());
    }

}
