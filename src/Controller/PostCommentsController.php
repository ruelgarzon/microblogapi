<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Http\Client;

/**
 * Post Comments Controller
 *
 * @property \App\Model\Table\PostCommentsTable $PostLikes
 */
class PostCommentsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Security->setConfig(
            'unlockedActions', 
            ['writeComment','deleteComment','editComment']
        );
    }
    
    /**
     * Write Comment method
     *
     * This method saves user inputed comments.
     *
     * @return json
     */
    public function writeComment() 
    {
        $status = 'error';
        $commentParam = '';
        $message = '';
        $id = 0;

        $data = $this->getCleanData();
        $data['user_id'] = $this->Auth->User('id');
        $accessToken = $this->Auth->User('token');

        $http = new Client(
            [
            'headers' => ['Authorization' => 'Bearer ' . $accessToken]
            ]
        );
        
        $response = $http->post(
            $this->baseUrl().'/api/PostComments/writeComment',
            $data,
            ['type' => 'json']
        );
        $result = $response->getJson();
        if ($result['code'] == $this->codeSuccessCreate201) {
            $status = 'success';
            $id = $result['data']['comment']['id'];
            $commentParam = h($data['comment']);
        } else {
            $message = $result['message'];
        }

        return $this->response
            ->withType('application/json')
            ->withStringBody(
                json_encode(
                    [
                    'status' => $status,
                    'id' => $id,
                    'comment' => $commentParam,
                    'message' => $message
                    ]
                )
            );
    }

    /**
     * Edit Comment method
     *
     * This method saves user inputed revision on comments.
     *
     * @return json
     */
    public function editComment() 
    {
        $status = 'error';
        $commentParam = '';

        $data = $this->getCleanData();

        $accessToken = $this->Auth->User('token');

        $http = new Client(
            [
            'headers' => ['Authorization' => 'Bearer ' . $accessToken]
            ]
        );
        
        $response = $http->post(
            $this->baseUrl().'/api/PostComments/editComment',
            $data,
            ['type' => 'json']
        );
        $result = $response->getJson();
        if ($result['code'] == $this->codeSuccess200) {
            $status = 'success';
            $commentParam = h($data['comment']);
        }

        return $this->response
            ->withType('application/json')
            ->withStringBody(
                json_encode(
                    [
                    'status' => $status,
                    'comment' => $commentParam
                    ]
                )
            );
    }

    /**
     * Delete Comment method
     *
     * This method removes user comments.
     *
     * @return json
     */
    public function deleteComment() 
    {
        $status = 'error';
        $commentParam = '';

        $data = $this->getCleanData();

        $accessToken = $this->Auth->User('token');

        $http = new Client(
            [
            'headers' => ['Authorization' => 'Bearer ' . $accessToken]
            ]
        );
        
        $response = $http->post(
            $this->baseUrl().'/api/PostComments/deleteComment',
            $data,
            ['type' => 'json']
        );
        $result = $response->getJson();
        if ($result['code'] == $this->codeSuccess200) {
            $status = 'success';
        }

        return $this->response
            ->withType('application/json')
            ->withStringBody(
                json_encode(
                    [
                    'status' => $status
                    ]
                )
            );
    }
}
