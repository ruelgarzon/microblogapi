<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class PostComment extends Entity
{

    protected $_accessible = [
        'id' => true,
        'user_id' => true,
        'post_id' => true,
        'comment' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true
    ];
}