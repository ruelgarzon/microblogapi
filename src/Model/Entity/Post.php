<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class Post extends Entity
{

    protected $_accessible = [
        'parent_id' => true,
        'user_id' => true,
        'content' => true,
        'image_name' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true
    ];
}

