<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class Follow extends Entity
{

    protected $_accessible = [
        'id' => true,
        'follower_id' => true,
        'followed_id' => true,
        'created' => true
    ];
}
