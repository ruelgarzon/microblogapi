<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class PostLike extends Entity
{

    protected $_accessible = [
        'id' => true,
        'user_id' => true,
        'post_id' => true,
        'created' => true
    ];
}
