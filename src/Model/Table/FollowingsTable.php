<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class FollowingsTable extends Table
{
   
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('follows');
        $this->setPrimaryKey('followed_id');

        $this->belongsTo(
            'Users', [
            'foreignKey' => 'followed_id'
            ]
        );
    }
}
