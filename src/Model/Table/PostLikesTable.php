<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class PostLikesTable extends Table
{

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('post_likes');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Posts')
            ->setForeignKey('post_id');

        $this->hasOne('Users')
            ->setForeignKey('user_id');
    }
}
