<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class UsersTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Posts')
            ->setForeignKey('user_id');

        $this->belongsTo('PostComments')
            ->setForeignKey('user_id');

        $this->hasMany(
            'UserFollowers', [
            'className' => 'Followers',
            'foreignKey' => 'follower_id'
            ]
        );

        $this->hasMany(
            'UserFollowings', [
            'className' => 'Followings',
            'foreignKey' => 'followed_id'
            ]
        );

        $this->hasOne(
            'UserFollowing', [
            'className' => 'Followings',
            'foreignKey' => 'followed_id'
            ]
        );
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->requirePresence('first_name')
            ->notEmptyString('first_name', 'First Name is required')
            ->add(
                'first_name', 'custom', [ 
                'rule' => ['custom', '/^[a-zA-Z ]+$/'], 
                'message' => 'First name do not allow numbers and special characters' 
                ]
            );

        $validator
            ->requirePresence('last_name')
            ->notEmptyString('last_name', 'Last Name is required')
            ->add(
                'last_name', 'custom', [ 
                'rule' => ['custom', '/^[a-zA-Z ]+$/'], 
                'message' => 'Last name do not allow numbers and special characters' 
                ]
            );

        $validator
            ->requirePresence('username')
            ->notEmptyString('username', 'Username is required')
            ->minLength('username', 8, 'Username must be atleast 8 characters')
            ->add(
                'username', 'unique', [
                'rule' => 'validateUnique',
                'provider' => 'table',
                'message' => 'Username is already in use. Please try another one'
                ]
            );

        $validator
            ->email('email', true, 'Please provide a valid email.')
            ->requirePresence('email', 'create')
            ->notEmptyString('email', 'Email is required')
            ->add(
                'email', 'unique', [
                'rule' => 'validateUnique',
                'provider' => 'table',
                'message' => 'Email is already in use. Please try another one'
                ]
            );

        $validator
            ->scalar('password')
            ->maxLength('password', 255)
            ->minLength('password', 8)
            ->requirePresence('password', 'create')
            ->notEmptyString('password', 'Password is required')
            ->sameAs('password', 'password_confirm', "Password doesn't match");

        $validator
            ->allowEmptyFile('profile_picture')
            ->add(
                'profile_picture', 'validExtension', [
                'rule' => ['extension', ['png', 'jpg', 'gif', 'jpeg']], 
                'message' => 'Please supply a valid image. 
                    Allowed extensions are only gif, jpeg, png and jpg.'
                ]
            );

        return $validator;
    }

    public function validationChangePass(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('password')
            ->maxLength('password', 255)
            ->minLength('password', 8)
            ->requirePresence('password', 'create')
            ->notEmptyString('password', 'Password is required')
            ->sameAs('password', 'password_confirm', "Password doesn't match");

        return $validator;
    }

    public function findAuth(\Cake\ORM\Query $query, array $options)
    {
        $query
            ->select(['id', 'username','email', 'password','first_name','last_name','profile_picture'])
            ->where(['Users.activated' => 1]);

        return $query;
    }
}
