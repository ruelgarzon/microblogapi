<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class PostCommentsTable extends Table
{

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('post_comments');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Posts')
            ->setForeignKey('post_id');

        $this->belongsTo('Users')
            ->setForeignKey('user_id');
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('user_id')
            ->notEmptyString('user_id', 'Invalid action. Cannot access session user id.');

        $validator
            ->requirePresence('comment')
            ->maxLength('comment', 140)
            ->notEmptyString('comment', 'Comment is required');

        return $validator;
    }
}
