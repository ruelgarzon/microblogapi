<?php 
    $userDetail = json_decode(json_encode($userDetail)); 
    $followings = json_decode(json_encode($followings)); 
?>
<div class="row p-5">
    <div class="col-sm-12 col-lg-3"></div>
    <div class="col-sm-12 col-lg-6 ">
        <div class="container shadow-lg bg-transparent">
            <div class="card bg-transparent border-0 pt-4">
                <?php 
                    if ($userDetail->profile_picture) {
                        $profile = $userDetail->profile_picture;
                    } else {
                        $profile = 'default-profile.png';
                    }
                ?>
              <?= $this->Html->image(
                            'profiles/'.$profile, 
                            ['alt' => '',
                                'class' => 'rounded-circle align-self-center border border-white',
                                'width' => '125',
                                'height' => '125'
                            ]
                        ) ?>
              <div class="card-body text-center pt-2">
                <span class="font-italic text-muted">@<?= $userDetail->username ?></span>
                <h5 class="card-title font-weight-bold route-user"
                    data-userId="<?= $userDetail->id ?> ">
                    <?= $userDetail->first_name ?>
                    <?= $userDetail->last_name ?>
                </h5>
              </div>
            </div>
        </div>
        <div class="container shadow-lg bg-transparent mt-2 py-2">
            <h5>Following
                <span class="badge badge-light ml-2">
                    <?php $this->Paginator->options($pagination) ?>
                    <?= $this->Paginator->params()['count'] ?>
                </span>
            </h5>
            <hr>
            <div class="row">
                <div class="col-sm-12" >
                    <?php if ($followings): ?>
                    <?php foreach ($followings as $key => $following): ?>
                    <div class="card">
                        <div class="card-body py-2">
                            <?php 
                                if ($following->user->profile_picture) {
                                    $profile = $following->user->profile_picture;
                                } else {
                                    $profile = 'default-profile.png';
                                }
                            ?>
                            <?= $this->Html->image(
                                'profiles/'.$profile, 
                                ['alt' => '',
                                    'class' => 'rounded-circle d-inline-block',
                                    'width' => '50',
                                    'height' => '50'
                                ]
                            ) ?>
                            <div class="d-inline-block align-bottom pl-2">
                                <h6 class="font-weight-bold m-0 route-user"
                                    data-userId="<?= $following->user->id ?> ">
                                    <?= $following->user->first_name ?> 
                                    <?= $following->user->last_name ?>
                                </h6>
                                <h6 class="small font-italic text-muted">
                                    @<?= $following->user->username ?>
                                </h6>
                            </div>
                            <?php 
                                if ($following->user->id == $authUser['id']) {
                                    $displayClass = 'd-none';
                                } else {
                                    $displayClass = 'd-inline-block';
                                }
                            ?>
                            <div class="<?= $displayClass ?> align-middle float-right line-hieght-50">
                                <?php 
                                    if (isset($following->user->user_following->id)) {
                                        $followLabel = 'Unfollow';
                                        $class = 'btn-secondary px-2';
                                    } else {
                                        $followLabel = 'Follow';
                                        $class = 'btn-outline-secondary px-3';
                                    }
                                ?>
                                <?= $this->Form->postButton(
                                    $followLabel,
                                    [
                                        'controller' => 'follows',
                                        'action' => 'follow',
                                        $following->user->id
                                    ],
                                    ['class' => "btn btn-sm {$class}"]
                                ) ?>
                            </div>
                        </div>
                    </div>
                    <?php endforeach ?>
                    <?= $this->element('common/pagination') ?>
                    <?php else: ?>
                        <div class="card">
                            <div class="card-body text-center  mt-4">
                                <p class="card-text">
                                    User seem to be not following someone. 
                                </p>
                            </div>
                        </div>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-lg-3"></div>
</div>
<?= $this->Html->script('follows') ?>