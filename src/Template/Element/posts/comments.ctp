<div class="card border-0">
    <div class="card-body py-2">
        <div class="d-inline-block align-bottom pl-2 likeContainer">
            <span data-id = "<?= $post->id ?>"
                data-likeCount = "<?= $comment['likeCount'] ?>"
                class="oi oi-heart post-action triggerLike 
                <?= ($comment['userLiked']) ? ' post-liked':'' ?>"></span>
            <?php if(empty($post->repost->id)):?>
            <span class="oi oi-loop-square post-action"
                onclick="Post.repost('<?=  $post->id ?>') "></span>
            <?php endif ?>
            <h6 class="small text-muted likeCount">
                <?= $comment['likeCount'] ?>
                <?= ($comment['likeCount'] > 1) ? ' Likes' : ' Like' ?> 
            </h6>

        </div>
        <div class="d-inline-block float-right">
            <br>
            <?php $commentClass = 'commentSection'.$post->id ?>
            <?php $commentCountId = 'commentCountId'.$post->id ?>
            <a data-toggle="collapse" 
                href="#<?= $commentClass ?>" role="button" aria-expanded="false" 
                aria-controls="<?= $commentClass ?>">
                <h6 class="small text-muted">
                    <span id="<?= $commentCountId ?>" 
                        data-commentCount = "<?= $comment['commentCount'] ?>">
                        <?= $comment['commentCount'] ?> 
                        <?= $comment['commentCount'] > 1 ? ' Comments' : ' Comment' ?> 
                    </span>
                    <span class="oi oi-elevator ml-2"></span>
                </h6>
            </a>
            
        </div>
        <div class="row">
            <div class="collapse w-100" id="<?= $commentClass ?>">
                <hr>
                <div class="list-group">
                    <?php if ($comment['comments']): ?>
                        <?php foreach($comment['comments'] as $comment): ?>
                        <?php $comment = json_decode(json_encode($comment)); ?>
                        <div class="list-group-item list-group-item-action flex-column
                            align-items-start line-hieght-10">
                            <div class="d-flex w-100 justify-content-between">
                                <small class="mb-1 font-weight-bold route-user"
                                    data-userId="<?= $comment->user_id ?>">
                                    <?= h($comment->user->first_name) ?> 
                                    <?= h($comment->user->last_name) ?> 
                                </small>
                                <small>
                                    <time class="timeago" 
                                        datetime="<?= $comment->created ?>">
                                    </time>
                                    <?php if($comment->user_id == 
                                        $authUser['id']) : ?>
                                    <div class="dropup d-inline-block">
                                        <a class="btn btn-link text-dark btn-sm" 
                                        href="#" 
                                        id="dropdownMenuButton" 
                                        data-toggle="dropdown" 
                                        aria-haspopup="true" aria-expanded="false">
                                            <span class="oi oi-ellipses small"></span>
                                        </a>
                                        <div class="dropdown-menu" 
                                        aria-labelledby="dropdownMenuButton">
                                            <a href="javascript:;" 
                                            class="dropdown-item line-hieght-10"
                                            onclick="Post.editComment(this,
                                                    <?= $comment->id ?>,
                                                    `<?= h($comment->comment) ?>`
                                                )">
                                                <span class="oi oi-pencil"></span> 
                                                Edit
                                            </a>
                                            <div class="dropdown-divider"></div>
                                            <a href="javascript:;" 
                                            class="dropdown-item line-hieght-10"
                                                onclick="Post.deleteComment(this,
                                                    <?= $comment->id ?>,
                                                    <?= $comment->post_id ?>
                                                )">
                                                <span class="oi oi-trash"></span> 
                                                Delete
                                            </a>
                                        </div>
                                    </div>
                                <?php endif ?>
                                </small>
                            </div>
                            <small class="font-weight-light" 
                                id="commentElement<?= $comment->id ?>">
                                <?= h($comment->comment) ?>
                            </small>
                        </div>
                        <?php endforeach ?>
                    <?php endif ?>
                    <div class="additionalDiv">
                        <div class="commentBox list-group-item
                            list-group-item-action flex-column
                            align-items-start line-hieght-10">
                            <div class="input-group mb-3">
                                <input name = "commentInput" type="text" 
                                class="form-control" 
                                    placeholder="Write comment here" 
                                    aria-label="Write comment here" 
                                    aria-describedby="basic-addon2"
                                    data-placement="top" data-toggle="popover" 
                                    data-content="Please provide a 
                                    comment before posting."
                                    data-trigger = 'manual'
                                    >
                                <div class="input-group-append">
                                    <button class="btn btn-outline-secondary
                                        postComment" 
                                        data-id = "<?= $post->id ?>"
                                        data-name = "<?= $authUser['first_name'].
                                        ' '.$authUser['last_name']?>"
                                        type="button">
                                        Post
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>