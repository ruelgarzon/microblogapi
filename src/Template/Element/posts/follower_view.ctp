<?php if ($followers): ?>
    <?php foreach ($followers as $key => $follower): ?>
    <div class="card">
        <div class="card-body py-2">
            <?php 
                if($follower->user->profile_picture) {
                    $profile = $follower->user->profile_picture;
                } else {
                    $profile = 'default-profile.png';
                }
            ?>
            <?= $this->Html->image(
                'profiles/'.$profile, 
                ['alt' => '',
                    'class' => 'rounded-circle d-inline-block',
                    'width' => '50',
                    'height' => '50'
                ]
            ) ?>
            <div class="d-inline-block align-bottom pl-2">
                <h6 class="font-weight-bold m-0 route-user"
                    data-userId="<?= $follower->user->id ?>">
                    <?= $follower->user->first_name ?> 
                    <?= $follower->user->last_name ?>
                </h6>
                <h6 class="small font-italic text-muted">
                    @<?= $follower->user->username ?>
                </h6>
            </div>
            <?php 
                if ($follower->user->id == $authUser['id']) {
                    $displayClass = 'd-none';
                } else {
                    $displayClass = 'd-inline-block';
                }
            ?>
            <div class="<?= $displayClass ?> align-middle float-right line-hieght-50">
                <?php 
                    if(isset($follower->user->user_following->id)) {
                        $followLabel = 'Unfollow';
                        $class = 'btn-secondary px-2';
                    } else {
                        $followLabel = 'Follow';
                        $class = 'btn-outline-secondary px-3';
                    }
                ?>
                <?= $this->Form->postButton(
                    $followLabel,
                    [
                        'controller' => 'follows',
                        'action' => 'follow',
                        $follower->user->id
                    ],
                    ['class' => "btn btn-sm {$class}"]
                ) ?>
                    
            </div>
        </div>
    </div>
    <?php endforeach ?>
<?php else: ?>
        <div class="card">
            <div class="card-body text-center  mt-4">
                <?php if ($editAccess): ?>
                    <p class="card-text">You don't have followers at the moment.</p>
                <?php else: ?>
                    <p class="card-text">The user doesn't have followers at the moment.</p>
                <?php endif ?>
            </div>
        </div>
<?php endif ?>
<?php if ($followerCount > 10): ?>
    <small class="form-text text-muted text-right">
        Showing only <b>10</b> of <b><?= $followerCount ?></b></small>
    <?= $this->Html->link(
        'View All Followers',
        ['controller' => 'follows', 'action' => 'followers/'.$follower->user->id],
        ['class' => 'btn btn-outline-info btn-block mt-3']
    ) ?>
<?php endif ?>