<div class="modal" id="postModal">
    <div class="modal-dialog">
        <div class="modal-content">
           
            <?= $this->Form->create('Post', 
                [
                    'enctype'=> 'multipart/form-data',
                    'id' => 'PostEditForm',
                    'url' => ['controller' => 'posts', 'action' => 'edit']
                ]
            ) ?>
                <?= $this->Form->control('id', ['type' => 'hidden']) ?>
                <div class="modal-header">
                    <h4 class="modal-title">Edit Post</h4>
                    <button type="button" class="close" data-dismiss="modal">
                        &times;
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <?=  $this->Form->control(
                            'content', 
                            [
                                'label' => 'Post Content',
                                'rows' => '4',
                                'class' => 'form-control'
                            ]
                        ) ?>
                    </div>
                    <div class="custom-file w-50">
                       <?=  $this->Form->file(
                            'image_name', 
                            [
                                'class' => 'custom-file-input',
                                'id' => 'post-attachment'
                            ]
                        ) ?>
                        <label class="custom-file-label text-truncate" for="post-attachment">
                        Choose image</label>
                    </div>
                    <div class="custom-control custom-checkbox d-inline align-middle">
                        <?=  $this->Form->checkbox(
                            'remove_image', 
                            [
                                'label' => false,
                                'class' => 'custom-control-input hide-post-attachment',
                                'id' => 'PostRemoveImage'
                            ]
                        ) ?>
                        <label class="custom-control-label" for="PostRemoveImage">No Image Attachment</label>
                    </div>
                </div>
                <div class="modal-footer">
                    <?= $this->Form->button( 'Save', [
                        'class' => 'btn btn-outline-primary'
                    ]) ?>
                </div>
            <?= $this->Form->end() ?>
            
        </div>
    </div>
</div>