<nav aria-label="..." class="mt-3">
    <ul class="pagination justify-content-end">
        <?php $this->Paginator->options($pagination) ?>
        <?php
            $this->Paginator->setTemplates([
                'prevActive' => '<li class="page-item"><a class="page-link" href="{{url}}">{{text}}</a></li>'
            ]);
            $this->Paginator->setTemplates([
                'prevDisabled' => '<li class="page-item disabled"><a class="page-link" href="{{url}}">{{text}}</a></li>'
            ]);
        ?>
        <?= $this->Paginator->prev(
                $this->Html->tag('span', '', 
                    ['class' => 'oi oi-caret-left']
                ),
                ['escape' => false]
            ) ?>
        <?php
            $this->Paginator->setTemplates([
                'number' => '<li class="page-item"><a class="page-link" href="{{url}}">{{text}}</a></li>'
            ]);
            $this->Paginator->setTemplates([
                'current' => '<li class="active page-item"><a class="page-link" href="">{{text}}</a></li>'
            ]);
        ?>
        <?= $this->Paginator->numbers() ?>
        <?php
            $this->Paginator->setTemplates([
                'nextActive' => '<li class="page-item"><a class="page-link" href="{{url}}">{{text}}</a></li>'
            ]);
            $this->Paginator->setTemplates([
                'nextDisabled' => '<li class="page-item disabled"><a class="page-link" href="{{url}}">{{text}}</a></li>'
            ]);
        ?>
        <?= $this->Paginator->next(
                $this->Html->tag('span', '', 
                    ['class' => 'oi oi-caret-right']
                ),
                ['escape' => false]
            ) ?>
    </ul>
</nav>
