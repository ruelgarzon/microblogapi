var Post = function () 
{
    var init = function () 
    {
        Common.uploadInit();
        Common.paginateInit();
        postComment();
        postLike();
    }

    var postComment = function () 
    {
        $('[data-toggle="popover"]').popover();
        $('.postComment').off('click').on('click', function() { 

            var $commentInput = $(this).closest("div.commentBox").find("input[name='commentInput']");
            var $additionalDiv = $(this).closest("div.additionalDiv");
            var $this = $(this);
            let id = $(this).attr("data-id");
            let name = $(this).attr("data-name");
            let comment = $commentInput.val();
            var addedDate = Common.getCurrentDateTime();
            

            if (comment != '') {
                var webroot = $('#webroot').val();
                $commentInput.val('');

                $.ajax({
                    url: webroot + 'postComments/writeComment',
                    type: 'post',
                    data: {
                        'post_id' : id,
                        'comment' : comment
                    },
                    dataType: 'json',
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                    }, 
                    success: function (result) {
                        if (result.status == 'success') {
                            updateCommentCount(id, 1);
                            var commentSection = commentTemplate (name, addedDate, result.id, id, result.comment);
                            $additionalDiv.prepend(commentSection);
                            Common.timeInit();
                        } else {
                            Common.notifMsg(result.message, 'error');
                        }
                    }
                });
                
            } else {
                $commentInput.popover('show');
                $commentInput.focus();
                $commentInput.focusout(function() {
                    $commentInput.popover('hide');
                });
            }
        });
    }
    var commentTemplate = function (name, addedDate, commentId, postId, comment) 
    {
        var commentSection = `<div class="list-group-item list-group-item-action flex-column
            align-items-start line-hieght-10">
            <div class="d-flex w-100 justify-content-between">
                <small class="mb-1 font-weight-bold">
                    ${name}
                </small>
                <small>
                    <time class="timeago" datetime="${addedDate}"></time>
                    <div class="dropup d-inline-block">
                        <a class="btn btn-link text-dark btn-sm" 
                        href="#" 
                        id="dropdownMenuButton" 
                        data-toggle="dropdown" 
                        aria-haspopup="true" aria-expanded="false">
                            <span class="oi oi-ellipses small"></span>
                        </a>
                        <div class="dropdown-menu" 
                        aria-labelledby="dropdownMenuButton">
                            <a href="javascript:;" 
                            class="dropdown-item line-hieght-10"
                            onclick="Post.editComment(this,
                                    ${commentId},
                                    \`${comment}\`
                                )">
                                <span class="oi oi-pencil"></span> 
                                Edit
                            </a>
                            <div class="dropdown-divider"></div>
                            <a href="javascript:;" 
                            class="dropdown-item line-hieght-10"
                                onclick="Post.deleteComment(this,
                                    ${commentId},
                                    ${postId}
                                )">
                                <span class="oi oi-trash"></span> 
                                Delete
                            </a>
                        </div>
                    </div>
                </small>
            </div>
            <small class="font-weight-light"
            id="commentElement${commentId}">
                ${comment}
            </small>
        </div>`;

        return commentSection;
    }

    var postLike = function () 
    {
        
        $('.triggerLike').off('click').on('click', function() 
        { 
            let id = $(this).attr("data-id");
            var button = $(this);

            let likeCount = parseInt(button.attr("data-likeCount"));
            if (button.hasClass('post-liked')) {
                button.removeClass('post-liked');
                likeCount -= 1;
            } else {
                button.addClass('post-liked');
                likeCount += 1;
            }
            let likeLabel = (likeCount > 1) ? ' Likes' : ' Like';
            button.attr('data-likeCount',likeCount);
            button.closest('.likeContainer').find('.likeCount').html(likeCount + likeLabel);
            
            var webroot = $('#webroot').val();

            $.ajax({
                url: webroot + 'postLikes/like',
                type: 'post',
                data: {
                    'post_id' : id
                },
                dataType: 'json',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                }, 
                success: function (result) {
                    if (result.status == 'success') {
                        
                    } else {
                        Common.notifMsg(result.message, 'error');
                    }
                }
            });
        });
    }

    var deleteComment = function (that, id, post_id) 
    {
        var webroot = $('#webroot').val();
        $.ajax({
            url: webroot + 'postComments/deleteComment',
            type: 'post',
            data: {'id' : id},
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
            }, 
            success: function (result) {
                if (result.status == 'success') {
                    updateCommentCount(post_id, 0);
                    $(that).closest("div.list-group-item").remove();
                }
            }
        });
           
    }
    var updateCommentCount = function (id, add) 
    {
        var commentCntContainer = $('#commentCountId'+id);
        let commentCount = parseInt($('#commentCountId'+id).attr("data-commentCount"));
        
        if (add) {
            commentCount += 1;
        } else {
            commentCount -= 1;
        }
        var countLable = (commentCount > 1) ? ' Comments' : ' Comment';

        commentCntContainer.attr("data-commentCount",commentCount);
        commentCntContainer.html(commentCount+countLable);
    }

    var edit = function (id, content) 
    {
        $('#postModal #id').val(id);
        $('#postModal #content').val(content);

        $('.hide-post-attachment').off('change').on('change', function () {
            var $checked = $(this).is(":checked");
            if ($checked === true) {
                $(this).closest('#postModal').find('div.custom-file').addClass('d-none');
            } else {
                $(this).closest('#postModal').find('div.custom-file').removeClass('d-none');
            }
        });

        $('#postModal').modal();
    }

    var editComment = function (that, id, content) 
    {
        $('#editComment #CommentId').val(id);
        $('#editComment #CommentContent').val(content);
        $('#editComment').modal();


        $('#editCommentForm').off( "submit.update").on( "submit.update",function(e) {
            e.preventDefault();
            var data = $(this).serialize();
            var webroot = $('#webroot').val();

            $.ajax({
                url: webroot + 'postComments/editComment',
                type: 'post',
                data: data,
                dataType: 'json',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                }, 
                success: function (result) {
                    if (result.status == 'success') {
                        $(that).attr('onClick',`Post.editComment(this, ${id},\`${result.comment}\`)`);
                        $('#commentElement' + id).html(result.comment);
                        $('#editComment').modal('hide');
                    } else {
                        Common.notifMsg(result.message, 'error');
                    }
                    $('#editComment').find('button[type="submit"]').attr('disabled',false);
                }
            });
        });
    }
    var repost = function (id) 
    {
        $('#repostModal #parent-id').val(id);
        $('#repostModal').modal();
    }

    return {
        
        init : function () {
            init();
        },
        edit : function (id, content) {
            edit(id, content);
        },
        deleteComment : function (that, id, post_id) {
            deleteComment(that, id, post_id);
        },
        editComment : function (that, id, content) {
            editComment(that, id, content);
        },
        repost : function (id) {
            repost(id);
        }

    }
    
}();

Post.init();