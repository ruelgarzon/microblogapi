var Follows = function () 
{
    var init = function () 
    {
        Common.paginateInit();
    }

    return {
        
        init : function () {
            init();
        }
    }
    
}();

Follows.init();